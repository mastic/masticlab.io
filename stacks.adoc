== Stacks

include::stack-quarkus.adoc[]

include::stack-angular.adoc[]

include::stack-spring.adoc[]
